﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FM5091_OutRef
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a number: ");
            string s = Console.ReadLine();
            double g;
            if (double.TryParse(s, out g))
            {
                Console.WriteLine("Your number plus 5 equals " + (g + 5.0));
            }
            else
            {
                Console.WriteLine("Sorry, you entered something that wasn't a number.  You're a disappointment.");
            }
            Console.ReadLine();

            int[] myarray = new int[5] { 50, 28, 99, 73, 4 };
            
            Console.WriteLine("Here is the contents of my array...");
            foreach(int i in myarray)
            {
                Console.WriteLine(i);
            }
            Console.ReadLine();

            //EuropeanOption e = new EuropeanOption();



            //Country USA = new Country()
            //{
            //    CapitalCity = "Washington DC",
            //    Name = "United States",
            //    Latitude = 323.23,
            //    Longitude = 39.384,
            //    YieldCurve
            //}
            //Exchange NASDAQ = new Exchange()
            //{
            //    Country = "USA",
            //    Name = "NASDAQ"
            //};
            //Underlying MSFT = new Underlying()
            //{
            //    Symbol = "MSFT",
            //    Exchange = ""
            //}

            // demonstrates the passing of an array back from a method
            double[] umn = PopulateArray();
            foreach (var d in umn)
                Console.WriteLine(d);

            // make a two dimensional array
            double[,] trinomial = new double[5, 5];
            // populate the array as a simple multiplication table
            for(int i = 0; i < 5; i++)
            {
                for(int j = 0; j < 5; j++)
                {
                    trinomial[i, j] = i * j;
                }
            }
            // print the array for the user
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    Console.Write(trinomial[i,j] + "\t");
                }
                Console.WriteLine();
            }

            Console.ReadLine();

            Console.WriteLine();
            Console.WriteLine("Making some students...");

            // make several instances of students with characteristics
            Student monty = new Student()
            {
                Age = 14,
                GPA = 2.39,
                ID = 1
            };

            Student lola = new Student()
            {
                Age = 15,
                GPA = 3.9,
                ID = 23
            };

            Student deckland = new Student()
            {
                Age = 33,
                GPA = 3.4,
                ID = 88
            };
            // make a collection to hold these students
            List<Student> my_class = new List<Student>();
            // ...add them
            my_class.Add(monty);
            my_class.Add(lola);
            my_class.Add(deckland);
            // count through each student object and print properties of them to the screen
            foreach(Student student in my_class)
            {
                Console.WriteLine(student.ID + ": " + student.GPA);
            }

            Console.ReadLine();

        }

        public static double[] PopulateArray()
        {
            return new double[5] { 1, 2, 3, 4, 5 };
        }
    }

    // a class to represent a simple student model
    public class Student
    {
        public int ID { get; set; }
        public int Age { get; set; }
        public double GPA { get; set; }
    }

    // classes supporting a data model necessary to price options
    public class Underlying
    {
        public string Symbol { get; set; }
        public Exchange Exchange { get; set; }
        public double Price { get; set; }
        public List<VolatilityPoint> VolatilitySurface { get; set; }
    }

    public class VolatilityPoint
    {
        public DateTime Expiration { get; set; }
        public double Volatility { get; set; }
    }

    public class Exchange
    {
        public string Name { get; set; }
        public Country Country { get; set; }
    }

    public class Country
    {
        public string Name { get; set; }
        public string CapitalCity { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public List<KeyRate> YieldCurve { get; set; }
    }

    public class KeyRate
    {
        public DateTime Tenor { get; set; }
        public double Rate { get; set; }
    }

    public class Option
    {
        public Underlying Underlying { get; set; }
        public DateTime ExpirationDate { get; set; }
    }

    public class EuropeanOption : Option
    {
        public double StrikePrice { get; set; }
        public bool IsCall { get; set; }

    }
}
